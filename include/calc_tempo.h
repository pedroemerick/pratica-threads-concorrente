/**
* @file	    calc_tempo.h
* @brief	Arquivo com as funções para o calculo de tempo dos algoritmos.
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	26/04/2017
* @date	    27/03/2018
*/

#ifndef CALC_TEMPO_H_
#define CALC_TEMPO_H_

#include <chrono>

#include "produto_matriz.h"
#include "funcoes_matriz.h"

/** 
 * @brief	Função genérica que calcula o tempo da multiplicação de matrizes na execução sequencial.
 * @param 	*tempo Variável que aponta para um vetor onde serão alocados os valores dos tempos calculados.
 * @param 	**matrizA Matriz usada para multiplicação.
 * @param 	**matrizB Matriz usada para multiplicação.
 * @param	n Dimensão da matriz.
 * @return Matriz produto da multiplicação
 */
template <typename T>
T** calc_tempo_I (double *tempo, T **matrizA, T **matrizB, int n) {

    T **matrizC;

    for (int kk = 0; kk < 20; kk++) {
        std::chrono::time_point <std::chrono::high_resolution_clock> start, end;

        start = std::chrono::high_resolution_clock::now ();
        matrizC = multiplicaI (matrizA, matrizB, n);
        end = std::chrono::high_resolution_clock::now ();

        std::chrono::duration <double> tempo_segundos = end - start;

        tempo [kk] = tempo_segundos.count ();

        delete_matriz (matrizC, n);        
    }

    matrizC = multiplicaI (matrizA, matrizB, n);

    return matrizC;
}

/** 
 * @brief	Função genérica que calcula o tempo da multiplicação de matrizes na execução concorrente.
 * @param 	*tempo Variável que aponta para um vetor onde serão alocados os valores dos tempos calculados.
 * @param 	**matrizA Matriz usada para multiplicação.
 * @param 	**matrizB Matriz usada para multiplicação.
 * @param	n Dimensão da matriz.
 * @return Matriz produto da multiplicação
 */
template <typename T>
T** calc_tempo_C (double *tempo, T **matrizA, T **matrizB, int n) {

    T **matrizC;

    for (int kk = 0; kk < 20; kk++) {
        std::chrono::time_point <std::chrono::high_resolution_clock> start, end;

        start = std::chrono::high_resolution_clock::now ();
        matrizC = multiplicaC (matrizA, matrizB, n);
        end = std::chrono::high_resolution_clock::now ();

        std::chrono::duration <double> tempo_segundos = end - start;

        tempo [kk] = tempo_segundos.count ();

        delete_matriz (matrizC, n);        
    }

    matrizC = multiplicaC (matrizA, matrizB, n);

    return matrizC;
}

#endif