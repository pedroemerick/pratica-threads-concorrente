/**
* @file	    produto_matriz.h
* @brief	Arquivo com as funções para multiplicação de matrizes na forma iterativa e concorrente 
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @author   Pedro Emerick (p.emerick@live.com)
* @since	26/04/2017
* @date	    27/03/2018
*/

#ifndef PRODUTO_MATRIZ_H_
#define PRODUTO_MATRIZ_H_

#include <thread>
using std::thread;

/**
* @brief Função que realiza a multiplicação de uma linha por uma coluna da matriz e
		 o armazena em uma posição da matriz C de acordo com a thread passada.
* @param **A Ponteiro de matriz que recebe o endereço de uma matriz.
* @param **B Ponteiro de matriz que recebe o endereço de uma matriz.
* @param **C Ponteiro de matriz que recebe o endereço de uma matriz.
* @param id Número da thread.
* @param dimensao Dimensao da matriz.
*/
void funcaoMultiplicaC (int **A, int **B, int **C, int id, int dimensao);

/**
* @brief Função que realiza o produto entre duas matrizes quadradas.
* @param **A Ponteiro de matriz que recebe o endereço de uma matriz.
* @param **B Ponteiro de matriz que recebe o endereço de uma matriz.
* @param n Dimensao da matriz
* @return Retorna uma matriz alocada dinâmicamente, com o resultado do produto entre duas matrizes A e B.
*/
template <typename T>
T** multiplicaI (T **A, T **B, int n) {

    T **matrizC = new T*[n];
    for(int ii = 0; ii < n; ii++)
        matrizC[ii] = new T[n];

    for (int ii = 0; ii < n; ii++) {

        for (int jj = 0; jj < n; jj++) {
            
            T soma = 0;

            for (int kk = 0; kk < n; kk++) {
                soma += ( A[ii][kk] * B[kk][jj] );  
            }   

            matrizC[ii][jj] = soma;
        }
    }

    return matrizC;
}

/**
* @brief Função que realiza o produto entre duas matrizes quadradas utilizando n threads,
         em que n é a dimensão da matriz, cada thread é responsável pela multiplicação
         entre uma linha e coluna, gerando uma linha da matriz resultante.
* @param **A Ponteiro de matriz que recebe o endereço de uma matriz.
* @param **B Ponteiro de matriz que recebe o endereço de uma matriz.
* @param n Dimensao da matriz
* @return Retorna uma matriz alocada dinâmicamente, com o resultado do produto entre duas matrizes A e B.
*/
template <typename T>
T** multiplicaC (T **A, T **B, int n) {

    T **matrizC = new T*[n];
    for(int ii = 0; ii < n; ii++)
        matrizC[ii] = new T[n];

    thread *threads = new thread [n];
    for (int ii = 0; ii < n; ii++) {
        threads[ii] = thread(funcaoMultiplicaC, A, B, matrizC, ii, n);
    }
    for (int ii = 0; ii < n; ii++) {
        threads[ii].join();
    }

    return matrizC;
}

#endif