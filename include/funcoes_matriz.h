/**
 * @file	funcoes_matriz.h
 * @brief	Implementação das funções que fazem operações com matrizes
 * @author	Pedro Emerick (p.emerick@live.com)
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	25/04/2017
 * @date	28/04/2017
 */

#ifndef FUNCOES_MATRIZ_H
#define FUNCOES_MATRIZ_H

/** 
 * @brief	Função genérica que libera uma matriz alocada dinamicamente
 * @param 	xx Matriz usada
 * @param	n Dimensão da matriz
 */
template < typename T >
void delete_matriz (T **xx, int n)
{
    for(int ii = 0; ii < n; ii++)
        delete [] xx[ii];

    delete [] xx;
}

/** 
 * @brief	Função genérica que soma duas matrizes e coloca o resultado em outra matriz
 * @param 	matrizX Matriz que recebe os valores resultantes da soma
 * @param 	matrizY Matriz usada para soma
 * @param 	matrizZ Matriz usada para soma
 * @param	n Dimensão das matrizes de cada partição
 */
template < typename T >
void soma_matrizes (T **matrizX, T **matrizY, T **matrizZ, int n)
{
    for (int ii = 0; ii < n; ii++) {
        for (int jj = 0; jj < n; jj++) {
            matrizX [ii][jj] = matrizY [ii][jj] + matrizZ [ii][jj];
        }
    }
}

#endif