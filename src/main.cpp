/**
* @file	    main.cpp
* @brief	Arquivo com a função principal do programa.
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	26/04/2017
* @date	    27/03/2018
*/

#include <iostream>
using std::cerr;
using std::endl;
using std::cout;

#include <string>
using std::string;

#include <cstdlib>
#include <algorithm>
#include <cctype>

#include "funcoes_arqs.h"
#include "funcoes_matriz.h"
#include "produto_matriz.h"
#include "calc_tempo.h"

/**
* @brief Função principal que realiza as chamadas das funções para a manipulação das matrizes e seus resultados.
* @param argc Variavel que contem a quantidade de argumentos passados via do terminal.
* @param *argv[] Vetor que contem os argumentos passados via do terminal.
*/
int main (int argc, char* argv[]) {

    /** Verificar se os argumentos passados pelo usuário são válidos */
    /** Verifica o número de argumentos passados */
    if (argc < 3 || argc > 13) {
        cout << "Necessário dimensao da matriz (de 2 a 2048) e se deseja a execução sequencial(S) ou concorrente(C)! Para que o ";
        cout << "programa funcione corretamente. EX.: ./bin/multimat 2 S" << endl;

        exit(1);
    }
    /** Verifica se os números passados são potencia de 2 e se o tipo de execução é valido */
    else {
        for (int ii = 1; ii < (argc-1); ii++) {
            if (atoi (argv[ii]) % 2 != 0) {
                cerr << argv[ii] << " Não é uma potencia de 2! Verifique o valor da dimensão e execute programa novamente!" << endl;
                exit(1);
            }     
        }

        string tipo_exec = argv[argc-1];
        transform (tipo_exec.begin(), tipo_exec.end(), tipo_exec.begin(), tolower);

        if (tipo_exec.compare("s") != 0 && tipo_exec.compare("c") != 0) {
            cout << "Tipo de execução invalido ! Use S para sequencial e C para concorrente !" << endl;
            exit(1);
        }
    }
    
    /** Pega o tipo de execução desejado */
    string tipo_exec = argv[argc-1];
    transform (tipo_exec.begin(), tipo_exec.end(), tipo_exec.begin(), tolower);

    /** Executa a multiplicação das matrizes de todas dimensões passadas */
    for (int ii = 1; ii < (argc-1); ii++) {

        int dimensao = atoi (argv[ii]);

        string nome_arq_A = "./data/input/A";
        nome_arq_A += argv[ii];
        nome_arq_A += "x";
        nome_arq_A += argv[ii];
        nome_arq_A += ".txt";
        
        int **matrizA = new int* [dimensao];
        for(int ii = 0; ii < dimensao; ii++)
            matrizA[ii] = new int [dimensao];

        carrega_arq (nome_arq_A, matrizA, dimensao);

        string nome_arq_B = "./data/input/B";
        nome_arq_B += argv[ii];
        nome_arq_B += "x";
        nome_arq_B += argv[ii];
        nome_arq_B += ".txt";

        int **matrizB = new int* [dimensao];
        for(int ii = 0; ii < dimensao; ii++)
            matrizB[ii] = new int [dimensao];

        carrega_arq (nome_arq_B, matrizB, dimensao);

        double tempo [20];

        int **matrizC;

        if (tipo_exec.compare("s") == 0) {
            matrizC = calc_tempo_I (tempo, matrizA, matrizB, dimensao);           
        } else {
            matrizC = calc_tempo_C (tempo, matrizA, matrizB, dimensao);
        }

        string nome_arq_C = "./data/output/C";
        nome_arq_C += argv[ii];
        nome_arq_C += "x";
        nome_arq_C += argv[ii];
        nome_arq_C += ".txt";

        cria_arq (nome_arq_C, matrizC, dimensao);
        cerr << "... Arquivo " << nome_arq_C << " gerado" << endl;
        
        saida_experimentacao (ii, tempo, dimensao, tipo_exec);

        delete_matriz (matrizA, dimensao);
        delete_matriz (matrizB, dimensao);
        delete_matriz (matrizC, dimensao);
    }

    return 0;
}
