/**
* @file	    produto_matriz.cpp
* @brief	Implementação da função chamada pelas threads para multiplicção da matriz
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	26/03/2018
* @date	    27/03/2018
*/

#include "produto_matriz.h"

/**
* @brief Função que realiza a multiplicação de uma linha por uma coluna da matriz e
		 o armazena em uma posição da matriz C de acordo com a thread passada.
* @param **A Ponteiro de matriz que recebe o endereço de uma matriz.
* @param **B Ponteiro de matriz que recebe o endereço de uma matriz.
* @param **C Ponteiro de matriz que recebe o endereço de uma matriz.
* @param id Número da thread.
* @param dimensao Dimensao da matriz.
*/
void funcaoMultiplicaC (int **A, int **B, int **C, int id, int dimensao) 
{
    int somaprod;

    for (int coluna = 0; coluna < dimensao; coluna++)
	{
		somaprod = 0;
		
		for(int ii = 0; ii < dimensao; ii++) 
		{
			somaprod += A[id][ii] * B[ii][coluna];
		}
		
		C[id][coluna] = somaprod;
	}
}