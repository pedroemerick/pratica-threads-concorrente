/**
* @file	    funcoes_arqs.cpp
* @brief	Implementação da função que gera arquivo de saída
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	26/04/2017
* @date	    27/03/2018
*/

#include "funcoes_arqs.h"

/**
* @brief Função que escreve o tempo de execução dos algoritmos em arquivos.
* @param ii Variavel que recebe a posição em que esta no vetor de parametros.
* @param *tempo Ponteiro que recebe um vetor com os tempos de execução do algoritmo de forma
         sequencial ou concorrente.
* @param n Variavel qe recebe a dimensao da matriz.
* @param tipo_exec Tipo de execução utilizada, sequencial ou concorrente
*/
void saida_experimentacao (int ii, double *tempo, int n, string tipo_exec) {

    /** Se for a primeira execução, cria o arquivo */
    if (ii == 1) {

        ofstream stats;

        /** Define o arquivo a ser usado, se será o de forma sequencial ou concorrente */
        if (tipo_exec.compare("s") == 0)
            stats.open ("./data/output/stats-seq.dat");
        else    
            stats.open ("./data/output/stats-con.dat");

        stats << "Dimensao" << setw(20) << "Maximo" << setw(20) << "Minimo" << setw(20) << "Médio" << setw(25) << "Desvio Padrão" << endl;
        
        stats << setprecision (7) << fixed;
        stats << n << setw(30) << maximo (tempo) << setw(20) << minimo (tempo) << setw(20) << media (tempo);
        stats << setw(20) << desvio_padrao (tempo, media (tempo)) << endl;
        stats.close ();
    }

    else {

        fstream stats;

        /** Define o arquivo a ser usado, se será o de forma sequencial ou concorrente */
        if (tipo_exec.compare("s") == 0)
            stats.open ("./data/output/stats-seq.dat", ios::in | ios::out);
        else
            stats.open ("./data/output/stats-con.dat", ios::in | ios::out);
        
        stats.clear ();
        stats.seekg (0, stats.end);
        
        stats << setprecision (7) << fixed;
        stats << n << setw(30) << maximo (tempo) << setw(20) << minimo (tempo) << setw(20) << media (tempo);
        stats << setw(20) << desvio_padrao (tempo, media (tempo)) << endl;
        stats.close ();
    }
}