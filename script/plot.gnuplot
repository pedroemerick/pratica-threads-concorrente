clear
reset
set key off

set terminal png size 1024, 768 enhanced font 'Helvetica, 12'
set output './images/Tempo_Medio.png'

set xlabel 'Dimensão da Matriz'
set ylabel 'Tempo'
set key box right top inside

set title 'Tempo médio de execução dos algoritmos'

set xrange [2:2048]

plot './data/output/stats-seq.dat' using 1:4 with lines linewidth 4 title 'Execução Sequencial', './data/output/stats-con.dat' using 1:4 with lines linewidth 4 title 'Execução Concorrente'


set output './images/Tempo_Minimo.png'

set title 'Tempo mínimo de execução dos algoritmos'

plot './data/output/stats-seq.dat' using 1:3 with lines linewidth 4 title 'Execução Sequencial', './data/output/stats-con.dat' using 1:3 with lines linewidth 4 title 'Execução Concorrente'

set output './images/Tempo_Maximo.png'

set title 'Tempo máximo de execução dos algoritmos'

plot './data/output/stats-seq.dat' using 1:2 with lines linewidth 4 title 'Execução Sequencial', './data/output/stats-con.dat' using 1:2 with lines linewidth 4 title 'Execução Concorrente'
